package com.example.keila.barometer;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import static com.github.mikephil.charting.components.Legend.LegendPosition.BELOW_CHART_CENTER;
import static com.github.mikephil.charting.components.Legend.LegendPosition.PIECHART_CENTER;


public class Reports extends  BaseActivity {

    PieChart pieChart;
    LineChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reports);


        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        setTitle(null);


        BottomNavigationView v = findViewById(R.id.navi);
        v.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        pieChart= findViewById(R.id.piechart);
        mChart =findViewById(R.id.linechart);









        pieChart.setUsePercentValues(true);
        pieChart.setTouchEnabled(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5,10,5,5);
        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(30f);
        pieChart.setRotationEnabled(false);
        pieChart.setTransparentCircleColor(R.color.white);






        ArrayList<PieEntry> values= new ArrayList<>();
        values.add(new PieEntry(25f,"Entediado"));
        values.add(new PieEntry(25f,"Motivado"));
        values.add(new PieEntry(25f,"Orgulhoso"));
        values.add(new PieEntry(25f,"Animado"));



        PieDataSet dataSet= new PieDataSet(values,"");
        dataSet.setSelectionShift(5f);
        dataSet.setSliceSpace(0f);
        dataSet.setColors(ColorTemplate.JOYFUL_COLORS);

        PieData data= new PieData((dataSet));
        data.setValueTextSize(12f);
        data.setValueTextColor(Color.BLACK);


        pieChart.setData(data);
        pieChart.setEntryLabelColor(Color.TRANSPARENT);


        Legend l = pieChart.getLegend();
        l.setPosition(BELOW_CHART_CENTER);

        //LINECHART

        mChart =(LineChart) findViewById(R.id.linechart);
        mChart.setDragEnabled(false);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);




        ArrayList<Entry> yvalues=new ArrayList<>();


        yvalues.add(new Entry(2,60f));
        yvalues.add(new Entry(1,50f));
        yvalues.add(new Entry(2,70f));
        yvalues.add(new Entry(3,30f));
        yvalues.add(new Entry(4,50f));
        yvalues.add(new Entry(5,60f));
        yvalues.add(new Entry(6,65f));


        LineDataSet set1= new LineDataSet(yvalues,"Experiência geral");
        set1.setFillAlpha(110);




        ArrayList<ILineDataSet> dataSets =new ArrayList<>();
        dataSets.add(set1);
        set1.setDrawHorizontalHighlightIndicator(false);

        LineData datas= new LineData(dataSets);
        mChart.setData(datas);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }



}