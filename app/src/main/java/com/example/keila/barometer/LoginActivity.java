package com.example.keila.barometer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;


public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }


    public void entre(View view) {
        Intent myIntent = new Intent(LoginActivity.this,DirectMessage.class);
        startActivity(myIntent);
    }
}

