package com.example.keila.barometer;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    EditText fecha;
    private int dia,mes,anio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fecha= findViewById(R.id.inicio);
        fecha.setOnClickListener(new View.OnClickListener() {




            @Override
            public void onClick(View v) {
                final Calendar c= Calendar.getInstance();
                if(v==fecha){
                    dia=c.get(Calendar.DAY_OF_MONTH);
                    mes=c.get(Calendar.MONTH);
                    anio=c.get(Calendar.YEAR);


                    DatePickerDialog datePicker= new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            (dayOfMonth+"/"+(month+1)+"/"+year);

                        }
                    }
                    ,dia,mes,anio);

                    datePicker.show();




                }

            }
        });





    }
}
