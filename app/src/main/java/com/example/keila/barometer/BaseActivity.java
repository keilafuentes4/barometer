package com.example.keila.barometer;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import static com.example.keila.barometer.R.id.nav_reports;


public abstract class BaseActivity extends AppCompatActivity {

    public BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            switch (item.getItemId()){
                case nav_reports:
                    startActivity(new Intent(BaseActivity.this,Reports.class));
                    break;
                case R.id.nav_messages:startActivity(new Intent(BaseActivity.this,Messages.class));
                    break;
            }
           return true;
        }

    };


}




