package com.example.keila.barometer;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.webkit.WebView;
import android.webkit.WebViewClient;



public class DirectMessage extends BaseActivity {

    String url="https://stackoverflow.com/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direct_message);


        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        setTitle(null);


        WebView web= (WebView) findViewById(R.id.webView1);
        web.setWebViewClient(new SendMessageWebView());
        web.loadUrl(url);

        BottomNavigationView v = findViewById(R.id.navi);
        v.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    static class SendMessageWebView extends WebViewClient {
        public boolean SeeWebView(WebView view,String url){
            view.loadUrl(url);
            return true;
        }
    }





}

